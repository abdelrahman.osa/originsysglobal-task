import React, { useMemo, useState, useCallback, useEffect } from 'react';
import MaterialReactTable from 'material-react-table';
import { Box, Button, Tooltip, IconButton } from '@mui/material';
import { ExportToCsv } from 'export-to-csv';
import FileDownloadIcon from '@mui/icons-material/FileDownload';
import { Delete, Edit } from '@mui/icons-material';
import { Axios } from './Axios/axios';
import classes from './App.module.css';


 function  App () {

const [tableData, setTableData] = useState([]);
const [layoutKey, setLayoutKey] = useState(false);

// call Api 
 useEffect(()=>{
   Axios(setTableData, setLayoutKey);  
},[])

// Table Headers 
const columns = useMemo(
  () => [
    {
      header: 'First Name',
      accessorKey: 'frist_name', //simple accessorKey pointing to flat data
    },
    {
        header: 'Last Name',
        accessorKey: 'last_name', //simple accessorKey pointing to flat data
      },
      {
        header: 'User Name',
        accessorKey: 'username', //simple accessorKey pointing to flat data
      },
      {
        header: 'Email',
        accessorKey: 'email', //simple accessorKey pointing to flat data
      }
  ],
  [],
);

// csv file options
const csvOptions = {
  fieldSeparator: ',',
  quoteStrings: '"',
  decimalSeparator: '.',
  showLabels: true,
  filename: 'Users-File',
  useBom: true,
  useKeysAsHeaders: false,
  headers: columns.map((col) => col.header),
};

const csvExporter = new ExportToCsv(csvOptions);

// Export Selected Rows
const handleExportRows = (rows) => {
  csvExporter.generateCsv(rows.map((row) => row.original));
};

// Export All Data
const handleExportData = () => {
  csvExporter.generateCsv(tableData);
};

// Edit Row Function
const handleSaveRow = async ({ exitEditingMode, row, values }) => {
  tableData[row.index] = values;
  setTableData([...tableData]);
  exitEditingMode(); //required to exit editing mode
};

// Delete Row Function
const handleDeleteRow =  useCallback(
  (row) => {
    if (
      !window.confirm(`Are you sure you want to delete ${row.original.frist_name}`)
    ) {
      return;
    }
    tableData.splice(row.index, 1);
    setTableData([...tableData]);
  },
  [tableData],
);

  return (
    <div>
      <p className={classes.Title}> User Data Management </p>

    {!layoutKey ? 
    //  Loader 
     <div className={classes.Loader}>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
      </div>
          : 
          // Table
          <MaterialReactTable
          columns={columns}
          data={tableData}
          enableGrouping
          enableRowSelection
          editingMode="modal"
          enableEditing
          onEditingRowSave={handleSaveRow}
          initialState={{
            pagination: { pageIndex: 0, pageSize: 5 }
          }}
          renderTopToolbarCustomActions={({ table }) => (
          <Box sx={{ display: 'flex', gap: '1rem', p: '0.5rem', flexWrap: 'wrap' }}>
            <Button
                color="primary"
                onClick={handleExportData}
                startIcon={<FileDownloadIcon />}
                variant="contained">
                Export All Data
              </Button>
    
              <Button
                disabled={
                  !table.getIsSomeRowsSelected() && !table.getIsAllRowsSelected()
                }
                onClick={() => handleExportRows(table.getSelectedRowModel().rows)}
                startIcon={<FileDownloadIcon />}
                variant="contained">
                Export Selected Rows
              </Button>
              </Box>
            )}
          renderRowActions={({ row, table }) => (
            <Box sx={{ display: 'flex', gap: '1rem' }}>
              <Tooltip arrow placement="left" title="Edit">
                <IconButton onClick={() => table.setEditingRow(row)}>
                  <Edit />
                </IconButton>
              </Tooltip>
              <Tooltip arrow placement="right" title="Delete">
                <IconButton color="error" onClick={() => handleDeleteRow(row)}> 
                  <Delete />
                </IconButton>
              </Tooltip>
            </Box>
            )}
        />
          }
    </div>
  );
}

export default App;
